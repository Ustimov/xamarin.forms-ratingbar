﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using RatingBar.FormsPlugin.Abstractions;

namespace RatingBarSample
{
    public class App : Application
    {
        public App()
        {
            var ratingBarControl = new RatingBarControl()
            {
                NumberOfStars = 6,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Rating = 3,
                IsEnabled = true,
                StepSize = 0.5f,
            };
            //ratingBarControl.RatingChanged += (s, e) => ((RatingBarControl)s).IsEnabled = false;

            // The root page of your application
            MainPage = new ContentPage
            {
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        new Label {
                            XAlign = TextAlignment.Center,
                            Text = "Welcome to Xamarin Forms!"
                        },
                        ratingBarControl,
                        //new Button
                        //{
                        //    Text = "Add Star",
                        //    Command = new Command(() => ratingBarControl.Rating++),
                        //}
                    }
                }
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
