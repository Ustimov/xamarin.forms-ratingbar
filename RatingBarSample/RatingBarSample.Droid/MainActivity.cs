﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
//using Android.Widget;
using Android.OS;
//using RatingBar.FormsPlugin.Abstractions;

namespace RatingBarSample.Droid
{
    [Activity(Label = "RatingBarSample", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            RatingBar.FormsPlugin.Android.RatingBarRenderer.Init();

            LoadApplication(new App());
        }
    }
}

