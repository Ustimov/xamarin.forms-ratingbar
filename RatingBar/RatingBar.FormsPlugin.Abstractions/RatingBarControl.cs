﻿using System;
using Xamarin.Forms;

namespace RatingBar.FormsPlugin.Abstractions
{
    /// <summary>
    /// RatingBar Interface
    /// </summary>
    public class RatingBarControl : View
    {
        public event EventHandler RatingChanged;
        
        public static readonly BindableProperty NumberOfStarsProperty =
          BindableProperty.Create<RatingBarControl, int>(p => p.NumberOfStars, 5);

        /// <summary>
        /// The number of stars (or rating items) to show. 
        /// </summary>
        public int NumberOfStars
        {
            get { return (int)GetValue(NumberOfStarsProperty); }
            set { SetValue(NumberOfStarsProperty, value); }
        }

        public static readonly BindableProperty RatingProperty =
            BindableProperty.Create<RatingBarControl, float>(p => p.Rating, 0);

        /// <summary>
        /// The rating to set by default. 
        /// </summary>
        public float Rating
        {
            get { return (float)GetValue(RatingProperty); }
            set
            {
                SetValue(RatingProperty, value);
                if (RatingChanged != null)
                {
                    RatingChanged(this, new EventArgs());
                }
            }
        }

        public static readonly BindableProperty StepSizeProperty =
            BindableProperty.Create<RatingBarControl, float>(p => p.StepSize, 1);

        /// <summary>
        /// The step size of the rating. 
        /// </summary>
        public float StepSize
        {
            get { return (float)GetValue(StepSizeProperty); }
            set { SetValue(StepSizeProperty, value); }
        }

        public static readonly new BindableProperty IsEnabledProperty =
            BindableProperty.Create<RatingBarControl, bool>(p => p.IsEnabled, true);

        /// <summary>
        /// The step size of the rating. 
        /// </summary>
        public new bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }
    }
}
