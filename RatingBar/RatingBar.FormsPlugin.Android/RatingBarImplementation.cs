using RatingBar.FormsPlugin.Abstractions;
using Xamarin.Forms;
using RatingBar.FormsPlugin.Android;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using RatingBarWidget = Android.Widget.RatingBar;
using System.ComponentModel;
using System;

[assembly: ExportRenderer(typeof(RatingBarControl), typeof(RatingBarRenderer))]

namespace RatingBar.FormsPlugin.Android
{
    /// <summary>
    /// RatingBar Renderer
    /// </summary>
    public class RatingBarRenderer : ViewRenderer<RatingBarControl, LinearLayout>, RatingBarWidget.IOnRatingBarChangeListener
    {
        /// <summary>
        /// Used for registration with dependency service
        /// </summary>
        public static void Init() { }

        public void OnRatingChanged(RatingBarWidget ratingBar, float rating, bool fromUser)
        {
            Element.Rating = rating;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<RatingBarControl> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            var linearLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
            var linearLayout = new LinearLayout(Forms.Context);
            linearLayout.LayoutParameters = linearLayoutParams;

            var ratingBar = new RatingBarWidget(Forms.Context)
            {
                NumStars = Element.NumberOfStars,
                Rating = Element.Rating,
                StepSize = Element.StepSize,
                IsIndicator = !Element.IsEnabled,
            };
            ratingBar.OnRatingBarChangeListener = this;

            // According to docs RatingBar should be inside a parent with width = wrap_content
            // for corrent number of stars showed
            // See http://developer.android.com/reference/android/widget/RatingBar.html for more information
            linearLayout.AddView(ratingBar);

            SetNativeControl(linearLayout);
        }

        
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Element == null || Control == null)
            {
                return;
            }

            var ratingBar = (RatingBarWidget)Control.GetChildAt(0);

            if (e.PropertyName == RatingBarControl.RatingProperty.PropertyName)
            {
                ratingBar.Rating = Element.Rating;
            }
            else if (e.PropertyName == RatingBarControl.IsEnabledProperty.PropertyName)
            {
                ratingBar.IsIndicator = !Element.IsEnabled;
            }
            else if (e.PropertyName == RatingBarControl.StepSizeProperty.PropertyName)
            {
                ratingBar.StepSize = Element.StepSize;
            }
            else if (e.PropertyName == RatingBarControl.NumberOfStarsProperty.PropertyName)
            {
                ratingBar.NumStars = Element.NumberOfStars;
            }
        }
    }
}