﻿using RatingBar.FormsPlugin.Abstractions;
using System;
using Xamarin.Forms;
using RatingBar.FormsPlugin.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RatingBar.FormsPlugin.Abstractions.RatingBarControl), typeof(RatingBarRenderer))]
namespace RatingBar.FormsPlugin.iOS
{
    /// <summary>
    /// RatingBar Renderer
    /// </summary>
    public class RatingBarRenderer //: TRender (replace with renderer type
    {
        /// <summary>
        /// Used for registration with dependency service
        /// </summary>
        public static void Init() { }
    }
}
