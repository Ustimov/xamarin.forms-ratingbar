﻿using RatingBar.FormsPlugin.Abstractions;
using System;
using Xamarin.Forms;
using RatingBar.FormsPlugin.WindowsPhone;
using Xamarin.Forms.Platform.WinPhone;

[assembly: ExportRenderer(typeof(RatingBar.FormsPlugin.Abstractions.RatingBarControl), typeof(RatingBarRenderer))]
namespace RatingBar.FormsPlugin.WindowsPhone
{
    /// <summary>
    /// RatingBar Renderer
    /// </summary>
    public class RatingBarRenderer //: TRender (replace with renderer type
    {
        /// <summary>
        /// Used for registration with dependency service
        /// </summary>
        public static void Init() { }
    }
}
